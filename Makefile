all:
	jekyll build --verbose --trace

clean:
	rm -rf _site

lint:
	@find -type f -not -wholename '*/_site/*' -print0 | xargs -0r grep -rl "href=[\"']/" | while read X; do \
		echo "W: $$X is using URIs that are not using '{{ \"/foo\" | prepend: site.baseurl }}'"; \
	done
